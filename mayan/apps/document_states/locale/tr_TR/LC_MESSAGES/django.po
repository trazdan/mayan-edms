# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# serhatcan77 <serhat_can@yahoo.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-08-03 01:22-0400\n"
"PO-Revision-Date: 2017-07-30 19:17+0000\n"
"Last-Translator: serhatcan77 <serhat_can@yahoo.com>\n"
"Language-Team: Turkish (Turkey) (http://www.transifex.com/rosarior/mayan-edms/language/tr_TR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr_TR\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: apps.py:44 queues.py:8
msgid "Document states"
msgstr "Belge durumları"

#: apps.py:69 apps.py:76
msgid "Current state of a workflow"
msgstr "Bir iş akışının geçerli durumu"

#: apps.py:70
msgid "Return the current state of the selected workflow"
msgstr "Seçilen iş akışının geçerli durumunu döndürür"

#: apps.py:77
msgid ""
"Return the completion value of the current state of the selected workflow"
msgstr "Seçili iş akışının geçerli durumunun tamamlanma değerini döndürür"

#: apps.py:92 models.py:38 models.py:96 models.py:157
msgid "Label"
msgstr "Etiket"

#: apps.py:95 models.py:35
msgid "Internal name"
msgstr "Dahili adı"

#: apps.py:99
msgid "Initial state"
msgstr "İlk durum"

#: apps.py:100 apps.py:110 apps.py:120 apps.py:126
msgid "None"
msgstr "Yok"

#: apps.py:104
msgid "Current state"
msgstr "Mevcut durum"

#: apps.py:108 apps.py:135 models.py:310
msgid "User"
msgstr "Kullanıcı"

#: apps.py:114
msgid "Last transition"
msgstr "Son geçiş"

#: apps.py:118 apps.py:131
msgid "Date and time"
msgstr "Tarih ve saat"

#: apps.py:124 apps.py:151 models.py:108
msgid "Completion"
msgstr "Tamamlama"

#: apps.py:138 forms.py:52 links.py:85 models.py:306
msgid "Transition"
msgstr "Geçiş"

#: apps.py:142 forms.py:55 models.py:312
msgid "Comment"
msgstr "Yorum Yap"

#: apps.py:147
msgid "Is initial state?"
msgstr "İlk durum mu?"

#: apps.py:155 models.py:161
msgid "Origin state"
msgstr "Kaynak Durum"

#: apps.py:159 models.py:166
msgid "Destination state"
msgstr "Hedef durum"

#: links.py:15 links.py:38 links.py:95 models.py:77 views.py:134 views.py:413
msgid "Workflows"
msgstr "İş Akışları"

#: links.py:20
msgid "Create workflow"
msgstr "İş akışı oluştur"

#: links.py:25 links.py:46 links.py:63
msgid "Delete"
msgstr "Sil"

#: links.py:29 models.py:42
msgid "Document types"
msgstr "Belge tipleri"

#: links.py:33 links.py:50 links.py:67
msgid "Edit"
msgstr "Düzenle"

#: links.py:41
msgid "Create state"
msgstr "Durum oluştur"

#: links.py:54 links.py:105
msgid "States"
msgstr "Durumlar"

#: links.py:58
msgid "Create transition"
msgstr "Geçiş oluştur"

#: links.py:71
msgid "Transitions"
msgstr "Geçişler"

#: links.py:77 queues.py:12
msgid "Launch all workflows"
msgstr "Tüm iş akışlarını başlat"

#: links.py:81
msgid "Detail"
msgstr "Detay"

#: links.py:90
msgid "Workflow documents"
msgstr "İş akışı belgeleri"

#: links.py:99
msgid "State documents"
msgstr "Durum belgeleri"

#: models.py:32
msgid ""
"This value will be used by other apps to reference this workflow. Can only "
"contain letters, numbers, and underscores."
msgstr "Bu değer, bu iş akışını referans olarak diğer uygulamalar tarafından kullanılacaktır. Yalnızca harf, rakam ve altçizgi içerebilir."

#: models.py:76 models.py:94 models.py:155 models.py:185
msgid "Workflow"
msgstr "İş Akışı"

#: models.py:100
msgid ""
"Select if this will be the state with which you want the workflow to start "
"in. Only one state can be the initial state."
msgstr "Bunun, iş akışının başlatılmasını istediğiniz durum olup olmayacağını seçin. Başlangıç ​​durumu yalnızca bir durum olabilir."

#: models.py:102
msgid "Initial"
msgstr "ilk"

#: models.py:106
msgid ""
"Enter the percent of completion that this state represents in relation to "
"the workflow. Use numbers without the percent sign."
msgstr "İş akışıyla ilişkili olarak bu durumun temsil ettiği tamamlama yüzdesini girin. Yüzde işareti olmadan rakamları kullanın."

#: models.py:114
msgid "Workflow state"
msgstr "İş akışı durumu"

#: models.py:115
msgid "Workflow states"
msgstr "İş akışı durumları"

#: models.py:177
msgid "Workflow transition"
msgstr "Iş akışı geçiş"

#: models.py:178
msgid "Workflow transitions"
msgstr "İş akışı geçişleri"

#: models.py:189
msgid "Document"
msgstr "belge"

#: models.py:284 models.py:299
msgid "Workflow instance"
msgstr "İş akışı örneği"

#: models.py:285
msgid "Workflow instances"
msgstr "İş akışı örnekleri"

#: models.py:302
msgid "Datetime"
msgstr "Tarih saat"

#: models.py:318
msgid "Workflow instance log entry"
msgstr "İş akışı örneği günlük girişi"

#: models.py:319
msgid "Workflow instance log entries"
msgstr "İş akışı örneği günlük girdileri"

#: models.py:323
msgid "Not a valid transition choice."
msgstr "Geçerli bir geçiş seçeneği değil."

#: models.py:329
msgid "Workflow runtime proxy"
msgstr "İş akışı çalışma zamanı vekili"

#: models.py:330
msgid "Workflow runtime proxies"
msgstr "İş akışı çalışma zamanı vekilleri"

#: models.py:336
msgid "Workflow state runtime proxy"
msgstr "İş akışı durum çalışma zamanı vekili"

#: models.py:337
msgid "Workflow state runtime proxies"
msgstr "İş akışı durum çalışma zamanı vekilleri"

#: permissions.py:7
msgid "Document workflows"
msgstr "Belge iş akışları"

#: permissions.py:10
msgid "Create workflows"
msgstr "İş akışları oluşturun"

#: permissions.py:13
msgid "Delete workflows"
msgstr "İş akışlarını sil"

#: permissions.py:16
msgid "Edit workflows"
msgstr "İş akışlarını düzenle"

#: permissions.py:19
msgid "View workflows"
msgstr "İş akışlarını görüntüle"

#: permissions.py:25
msgid "Transition workflows"
msgstr "İş akış geçişleri"

#: permissions.py:28
msgid "Execute workflow tools"
msgstr "İş akışı araçlarını çalıştır"

#: serializers.py:22
msgid "Primary key of the document type to be added."
msgstr "Belge türünün birincil anahtarı eklenecek."

#: serializers.py:37
msgid ""
"API URL pointing to a document type in relation to the workflow to which it "
"is attached. This URL is different than the canonical document type URL."
msgstr "Bağlı olduğu iş akışıyla ilişkili olarak bir doküman türünü işaret eden API URL'si. Bu URL, yasal çerçeve türü URL'inden farklı."

#: serializers.py:116
msgid "Primary key of the destination state to be added."
msgstr "Hedef durumun birincil anahtarı eklenecek."

#: serializers.py:120
msgid "Primary key of the origin state to be added."
msgstr "Kaynak durumun birincil anahtarı eklenecek."

#: serializers.py:218
msgid ""
"API URL pointing to a workflow in relation to the document to which it is "
"attached. This URL is different than the canonical workflow URL."
msgstr "Bağlı olduğu dokümana ilişkin bir iş akışını işaret eden API URL'si. Bu URL, kurallı iş akışı URL'inden farklı."

#: serializers.py:227
msgid "A link to the entire history of this workflow."
msgstr "Bu iş akışının tüm geçmişi ile bağlantı."

#: serializers.py:259
msgid ""
"Comma separated list of document type primary keys to which this workflow "
"will be attached."
msgstr "Bu iş akışının ekleneceği belge türü birincil anahtarlarının virgülle ayrılmış listesi."

#: serializers.py:319
msgid "Primary key of the transition to be added."
msgstr "Geçişin birincil anahtarı eklenecek."

#: views.py:53
#, python-format
msgid "Workflows for document: %s"
msgstr "Belge için iş akışı: %s"

#: views.py:77
#, python-format
msgid "Detail of workflow: %(workflow)s"
msgstr "İş akışının ayrıntısı: %(workflow)s"

#: views.py:101
#, python-format
msgid "Document \"%s\" transitioned successfully"
msgstr "\"%s\" dokümanı başarıyla değiştirildi"

#: views.py:110
msgid "Submit"
msgstr "Gönder"

#: views.py:112
#, python-format
msgid "Do transition for workflow: %s"
msgstr "İş akışı için geçiş yapın: %s"

#: views.py:164
msgid "Available document types"
msgstr "Kullanılabilir belge türleri"

#: views.py:165
msgid "Document types assigned this workflow"
msgstr "Bu iş akışına atanan belge türleri"

#: views.py:175
#, python-format
msgid "Document types assigned the workflow: %s"
msgstr "İş akışına atanan belge türleri: %s"

#: views.py:214 views.py:498
#, python-format
msgid "States of workflow: %s"
msgstr "Iş akışı durumları: %s"

#: views.py:232
#, python-format
msgid "Create states for workflow: %s"
msgstr "İş akışı için durumlar oluşturun: %s"

#: views.py:308
#, python-format
msgid "Transitions of workflow: %s"
msgstr "İş akışının geçişleri: %s"

#: views.py:321
#, python-format
msgid "Create transitions for workflow: %s"
msgstr "İş akışı için geçişler oluşturun: %s"

#: views.py:351
msgid "Unable to save transition; integrity error."
msgstr "Geçiş kaydedilemedi; Bütünlük hatası."

#: views.py:440
#, python-format
msgid "Documents with the workflow: %s"
msgstr "İş akışına sahip belgeler: %s"

#: views.py:461
#, python-format
msgid "Documents in the workflow \"%s\", state \"%s\""
msgstr "\"%s\" iş akışındaki belgeler, \"%s\" durumu"

#: views.py:512
msgid "Launch all workflows?"
msgstr "Tüm iş akışlarını başlatmak istiyor musunuz?"

#: views.py:519
msgid "Workflow launch queued successfully."
msgstr "İş akışı başlatma başarıyla sıraya girdi."
