# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-08-03 01:23-0400\n"
"PO-Revision-Date: 2015-08-20 19:07+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: English (http://www.transifex.com/rosarior/mayan-edms/"
"language/en/)\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:18 links.py:31 links.py:35 permissions.py:7 views.py:36
msgid "Events"
msgstr "Events"

#: apps.py:27
msgid "Timestamp"
msgstr "Timestamp"

#: apps.py:29
msgid "Actor"
msgstr "Actor"

#: apps.py:31
msgid "Verb"
msgstr "Verb"

#: classes.py:23
#, python-brace-format
msgid "Unknown or obsolete event type: {0}"
msgstr "Unknown or obsolete event type: {0}"

#: models.py:13
msgid "Name"
msgstr "Name"

#: models.py:17
msgid "Event type"
msgstr "Event type"

#: models.py:18
msgid "Event types"
msgstr "Event types"

#: permissions.py:9
msgid "Access the events of an object"
msgstr "Access the events of an object"

#: views.py:29 views.py:84
msgid "Target"
msgstr "Target"

#: views.py:69
#, python-format
msgid "Events for: %s"
msgstr "Events for: %s"

#: views.py:92
#, python-format
msgid "Events of type: %s"
msgstr "Events of type: %s"
